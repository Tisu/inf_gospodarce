var selectedFilters = new Set();
var MAIN_NODE = 'main_node';
var END_NODE = 'end_node';
var MIDDLE_NODE = 'middle_node';
var colors = {
	"hiponimia":"yellow",
	"część":"#f8f8ff",
	"miejsce":"#ffd39b",
	"egzemplarz":"blue",
	"Syn_plWN-PWN":"red",
	"typ":"#8b8878",
	"fuzzynimia_synsetów":"#758a81",
	"hiperonimia" : "#e58c39",
	"pot_odp_PWN-plWN":"#bb826c",
	"Syn_PWN-plWN" : "#19e8ee",
	"bliskoznaczność":"#8471ff",
	"hipo_plWN-PWN":"#9f191f"
};
var MAIN_RELATIONS_COLOR = "#d6f4b4";
var MAIN_NODE_SIZE = '70';
var LINE_WIDTH = 3;

function drawGraph(cy,data) {
	if(data== undefined){
		loading.classList.add('loaded');
		alert("Slowo nie znajduje sie w slowosieci");
		log("null response for server");
		return;
	}
	graph_data_cache=data;
	console.log('Informacje o słowie z serwera');
	console.log(JSON.stringify(data));

	var collection = cy.elements("node");
	cy.remove(collection);

	drawVertexes(cy,data);

	//graf domyslnie ma wysokosc 0px, dopiero gdy pojawiaja sie dane to zwieksza sie jego rozmiar
	document.getElementById('cy').style.height = '800px';
	cy.resize();
	//jeśli za dużo słów ustaw kratę, jeśli nie to 'słoneczko'
	cy.layout({name : 'cose'});
};
function filterRelations(unique_relations,relation){
	return unique_relations.has(relation.name) || !selectedFilters.has(relation.name) ;
};
function drawVertexes(cy,data){
		var meaning = data;
		var relations = meaning.relations;
		cy.add([{group: "nodes", data:{id: meaning.id, name: meaning.word, type:MAIN_NODE,definition:meaning.definition},style: {width: MAIN_NODE_SIZE,height: MAIN_NODE_SIZE}}]);
		drawRelations(cy,relations,meaning.id)
}
function  drawRelations(cy,relations,mainVertexId) {
	var unique_relations = new Set();
	for(var j = 0; j < relations.length; j++){
		var relation = relations[j];
		var rel_id = randomInt();
		if(filterRelations(unique_relations,relation)){
			continue;
		}
		unique_relations.add(relation.name);
		cy.add([
			{group: "nodes", style: {'background-color': MAIN_RELATIONS_COLOR},data:{id: rel_id, name: relation.name, type:MIDDLE_NODE}},
			{ group: "edges", style: {'line-color': MAIN_RELATIONS_COLOR, width:LINE_WIDTH},data: { source: rel_id, target: mainVertexId}}
		]);
		drawNodes(cy,relation,rel_id);
	}
};
function drawNodes(cy,relation,rel_id){
	var relation_color = colors[relation.name];
	var unique_item = new Set();
	for(var k = 0; k < relation.items.length; k++){
		var item = relation.items[k];
		if(unique_item.has(item.word)){
			continue;
		}
		unique_item.add(item.word);
		var item_id = randomInt();
		cy.add([
			{ group: "nodes",style: {'background-color': relation_color}, data:{id: item_id, name: item.word, wordnetId:item.wordId, type:END_NODE}},
			{ group: "edges",style: {'line-color': relation_color, width:LINE_WIDTH}, data: { source: item_id, target: rel_id}}
		]);
	}
};
function drawSearchWords(cy, data){
	log('Informacje o słowie z serwera');
	log(JSON.stringify(data));
	log(data);

	var collection = cy.elements("node");
	cy.remove(collection);

	for(var i = 0; i < data.definitions.length; i++){
		var meaning = data.definitions[i];
		cy.add([{group: "nodes", data:{id: meaning.id,wordnetId: meaning.id, name: meaning.word, type: END_NODE}}]);
	}
	//graf domyslnie ma wysokosc 0px, dopiero gdy pojawiaja sie dane to zwieksza sie jego rozmiar
	document.getElementById('cy').style.height = data.definitions.length *100 +'px';
	cy.resize();
	//jeśli za dużo słów ustaw kratę, jeśli nie to 'słoneczko'
	cy.layout({name : 'grid'});
};
var layoutPadding = 50;
var layoutDuration = 500;

function clickAnimation(cy, node ){
	loading.classList.add('loaded');
	var nhood = node.closedNeighborhood();
	if( nhood.length < 4)
	{
		return false;
	}
	cy.batch(function(){
		cy.elements().not( nhood ).removeClass('highlighted').addClass('faded');
		nhood.removeClass('faded').addClass('highlighted');

		var npos = node.position();
		var w = window.innerWidth;
		var h = window.innerHeight;

		cy.stop().animate({
			fit: {
				eles: cy.elements(),
				padding: layoutPadding
			}
		}, {
			duration: layoutDuration
		}).delay( layoutDuration, function(){
			nhood.layout({
				name: 'concentric',
				padding: layoutPadding,
				animate: true,
				animationDuration: layoutDuration,
				boundingBox: {
					x1: npos.x - w/2,
					x2: npos.x + w/2,
					y1: npos.y - w/2,
					y2: npos.y + w/2
				},
				fit: true,
				concentric: function( n ){
					if( node.id() === n.id() ){
						return 2;
					} else {
						return 1;
					}
				},
				levelWidth: function(){
					return 1;
				}
			});
		} );

	});
	return true;
}

function randomInt() {
	return Math.floor(Math.random() * (10000 - 1 + 1)) + 1;
};
function log(value) {
	if (LOG_TO_CONSOLE) {
		console.log(value);
	}
}