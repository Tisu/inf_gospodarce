var LOG_TO_CONSOLE = true;
var DISABLE_RELATIONS_SEARCH=true;
var graph_data_cache= "";
(function (){
	var app = angular.module('wordnetApp', []);
  app.controller('WordnetAppController',['$http', '$log', function($http, $log){
    var vm = this;
    vm.info = projectInfo;
    vm.image = background_img;
    vm.input = "";
    vm.wordInfo = [];
    vm.nothingFound = false;
    vm.graphReady = false;
    var loading= document.getElementById('loading');
    loading.classList.add('loaded');
    AddAllRelationsToFilter();

	
//kod grafu
var cy = cytoscape({
  container: document.getElementById('cy'), //inicjalizacja
  boxSelectionEnabled: false,
  autounselectify: true,

  style: cytoscape.stylesheet()
  .selector('node')
  .css({
    'content': 'data(name)',
    'text-valign': 'center',
    'color': 'white',
    'text-outline-width': 1,
    'text-outline-color': 'black',
    'background-color': '#42dca3'
  })
  .selector(':selected')
  .css({
    'background-color': 'black',
    'line-color': 'black',
    'target-arrow-color': 'black',
    'source-arrow-color': 'black',
    'text-outline-color': 'black'
  }),

  layout: {
    padding: 10
  }
});

cy.on('mousemove','node', function(event) {
      var evtTarget = event.cyTarget;

      var node_data = evtTarget.data();
     if(node_data.type == MAIN_NODE){
		 evtTarget.qtip({
					content: function(){ return node_data.definition },
					position: {
						my: 'top center',
						at: 'bottom center'
					},
					style: {
						classes: 'qtip-bootstrap',
						tip: {
							width: 16,
							height: 8
						}
					}
				});
     }
  });

  
  

  
      //funkcja opisujaca akcje po kliknieciu w wierzcholek grafu
  cy.on('tap', 'node', function(event){
  loading.classList.remove('loaded');
   var evtTarget = event.cyTarget;

   if( evtTarget === cy ){
   } else {
    if(evtTarget.data().type == END_NODE){
     getData(evtTarget.data().wordnetId);
     return;
   }
   if(DISABLE_RELATIONS_SEARCH){
    if(!clickAnimation(cy, this )){
     vm.getData(evtTarget.data().wordnetId);
   }
 }else {
  vm.getData(evtTarget.data().wordnetId);
}
}
});

vm.send = send;
vm.filter = filter;
vm.getData = getData;
vm.parseSentence = parseSentence;
vm.filterGraph = filterGraph;
vm.drawGraph = drawGraph;
// vm.fileInput = fileInput;

function displayContents(contents) {
  var element = document.getElementById('file-content');
  element.innerHTML = contents;
}

// function fileInput(){
	document.getElementById('file-input')
	  .addEventListener('change', readSingleFile, false);
	function readSingleFile(e) {
	  var file = e.target.files[0];
	  if (!file) {
	    return;
	  }
	  var reader = new FileReader();
	  reader.onload = function(e) {
	    var contents = e.target.result;
					vm.input=contents;
	    console.log(contents);
	    vm.parseSentence(contents);
	  };
	  reader.readAsText(file);
	}
// }

function filter() {
  console.log("filter called");
 bootbox.dialog({
  title: '<div class="dialog-text">Filtruj</div>',
  message: '<div><div class="dialog-text">Dostępne relacje: </div>'+
	'<div class="dialog-text">' +
  '<input type="checkbox" name="relations" id="relations-0" value="hiperonimia"> ' +
  'hiperonimia <br>' +
  '<input type="checkbox" name="relations" id="relations-1" value="hiponimia"> ' +
  'hiponimia <br>' +
  '<input type="checkbox" name="relations" id="relations-2" value="część"> ' +
  'część<br>' +
  '<input type="checkbox" name="relations" id="relations-3" value="synonimia"> ' +
  'synonimia <br>' +
  '<input type="checkbox" name="relations" id="relations-4" value="wytwór"> ' +
  'wytwór|rezultat <br>' +
  '<input type="checkbox" name="relations" id="relations-5" value="Syn_plWN-PWN"> ' +
  'Syn_plWN-PWN <br>' +
  '<input type="checkbox" name="relations" id="relations-6" value="Syn_PWN-plWN"> ' +
  'Syn_PWN-plWN <br>' +
  '<input type="checkbox" name="relations" id="relations-7" value="bliskoznaczność"> ' +
  'bliskoznaczność<br>' +
  '<input type="checkbox" name="relations" id="relations-8" value="czas"> ' +
  'czas<br>' +
  '</div></div> ',
  buttons: {
    success: {
      label: "Filtruj",
      className: "btn-success",
      callback: function() {
        selectedFilters.clear();
        var checkboxes = document.getElementsByName("relations");
        for (var i=0; i<checkboxes.length; i++) {
        if (checkboxes[i].checked) {
          selectedFilters.add(checkboxes[i].value);
        }
      }
      vm.filterGraph();
    }
  },
    all: {
      label: "Zaznacz wszystkie",
      className: "btn-all",
      callback: function() {
        selectedFilters.clear();
        var checkboxes = document.getElementsByName("relations");
          var all_boxes_checked = true;
          for (var i=0; i<checkboxes.length; i++) {
              if(checkboxes[i].checked == false){
                  all_boxes_checked = false;
                  break;
              }
          }
          if(all_boxes_checked) {
              for (var i = 0; i < checkboxes.length; i++) {
                  checkboxes[i].checked = false;
              }
          }
          else {
              for (var i = 0; i < checkboxes.length; i++) {
                  checkboxes[i].checked = true;
              }
          }
      return false;
    }
  }
}
}
);
var checkboxes = document.getElementsByName("relations");
        for (var i=0; i<checkboxes.length; i++) {
        checkboxes[i].checked = selectedFilters.has(checkboxes[i].value);
      }
};

function filterGraph() {
    drawGraph(cy,graph_data_cache);
  //place to draw graph again with appriopriate filtering
}

function send()
{
  log("Wysłanie tekstu");
  log(this.input);
  vm.wordInfo = [];
  vm.parseSentence(this.input);
  loading.classList.remove('loaded');
}

function parseSentence(input){
  log('parseSentence');
  $http.post('/parseSentence', {
    text: input
  }).success(function(data) {
      drawSearchWords(cy,data);
      vm.graphReady = true;
      loading.classList.add('loaded');
  }).error(function(msg){
    console.log(msg);
  });
}

function getData(input){
  log('getData');
  if(input < 0){
    alert("Brak informacji o relacjach słowa");
    return;
  }
  $http.post('/createModel', {
    id: input
}).success(function(data){
    drawGraph(cy,data);
    vm.graphReady = true;
    loading.classList.add('loaded');
      }).error(function(msg){
      	console.log(msg);
      });}
} ]);
function AddAllRelationsToFilter() {
    selectedFilters.add("hiperonimia");
    selectedFilters.add("hiponimia");
    selectedFilters.add("część");
    selectedFilters.add("synonimia");
    selectedFilters.add("wytwór");
    selectedFilters.add("Syn_plWN-PWN");
    selectedFilters.add("Syn_PWN-plWN");
    selectedFilters.add("bliskoznaczność");
    selectedFilters.add("czas");
}
  var background_img = 'url(../img/intro-bg.jpg)';
  var projectInfo = {
    courseName: 'Informatyka w gospodarce',
    projectName: 'Aplikacja do przeglądania słowosieci',
    autors: [
      {
        name: 'Mirek Karpiewski',
        contact: '200816@student.pwr.edu.pl',
        class: 'timeline-panel',
      },{
        name: 'Anna Nowak',
        contact: '200841@student.pwr.edu.pl',
        class: 'timeline-inverted',
      },{
        name: 'Piotr Szkudlarski',
        contact: '200996@student.pwr.edu.pl',
        class: 'timeline-panel',
      },{
        name: 'Bartłomiej Rodak',
        contact: '200839@student.pwr.edu.pl',
        class: 'timeline-inverted',
      },{
        name: 'Dominika Zięba',
        contact: '200042@student.pwr.edu.pl',
        class: 'timeline-panel',
      },{
        name: 'Izabela Gosławska',
        contact: '196205@student.pwr.edu.pl',
        class: 'timeline-inverted',
      }
    ],
  };
})();
