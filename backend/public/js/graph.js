$(function(){ // on dom ready

var cy = cytoscape({
  container: $('#cy')[0],
  
  boxSelectionEnabled: false,
  autounselectify: true,
  
  style: cytoscape.stylesheet()
    .selector('node')
      .css({
        'content': 'data(name)',
        'text-valign': 'center',
        'color': 'white',
        'text-outline-width': 2,
        'text-outline-color': '#888'
      })
    .selector(':selected')
      .css({
        'background-color': 'black',
        'line-color': 'black',
        'target-arrow-color': 'black',
        'source-arrow-color': 'black',
        'text-outline-color': 'black'
      }),
  
  elements: {
    nodes: [
      { data: { id: 'j', name: 'Jerry', href: 'http://cytoscape.org' } },
      { data: { id: 'e', name: 'Elaine', href: 'http://cytoscape.org'   } },
      { data: { id: 'k', name: 'Kramer', href: 'http://cytoscape.org'  } },
      { data: { id: 'g', name: 'George', href: 'http://cytoscape.org'  } }
    ],
    edges: [
      { data: { source: 'j', target: 'e' } },
        { data: { source: 'j', target: 'k' } },
        { data: { source: 'j', target: 'g' } }
    ]
  },
  
  layout: {
    name: 'circle',
    padding: 10
  }
});
  
cy.on('tap', 'node', function(){
  try { // your browser may block popups
    window.open( this.data('href') );
  } catch(e){ // fall back on url change
    window.location.href = this.data('href'); 
  } 
});

}); // on dom ready