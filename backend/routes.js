var fs = require('fs');
var wcd = require('./wcd')
var wordnet = require('./wordnet3')

module.exports = function(app){

	app.post('/text', function(req, res) {
		console.log(req.body);
		var path = require('path');
		fs.readFile(path.join(__dirname, 'public/model.json'), 'utf-8', function(err, data) {
			if(err){
				console.log("error: " + err);
				res.send(err);
			}else{
				res.set({
					'Content-Type': 'application/json'
				});
				console.log("data: " + JSON.parse(data));
				res.send(JSON.parse(data));
			}
			res.end();
		});
	});

	app.post('/createModel', function(req, res) {
		console.log(req.body.id);
		console.log('\n\n\n');
		wordnet.xmlToWordnet(req.body.id, function(result) {
			res.send(result);
			res.end();
		});
		// });
	});
	app.post('/parseSentence', function(req, res) {
		console.log(req.body.text);
		wcd.loadText(req.body.text,function(response) {
			console.log("response: " + response);
			res.send(response);
			res.end();
			// console.log('\n\n\n');
			// wordnet.xmlToWordnet(response, function(err, result) {
			// 	// console.log(result);
			// 	res.send(result);
			// 	res.end();
			// });
		});
	});

	// app.get('*', function(req, res) {
	// fs.readFile('public/index.html', 'utf-8', function(err, data) {
	// 		if(err){
	// 			res.send(err);
	// 		}else{
	// 			res.send(data);
	// 		}
	// 		res.end(data);
	// 		});
	// 	});		
};