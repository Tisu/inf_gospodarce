var request = require('request')
var reqP = require('request-promise');

var Promise = require('promise');
var BASE_URL = 'http://api.slowosiec.clarin-pl.eu:8080/plwordnet-api/'
var SENSES = 'senses/';
var SENSE = '/senses';
var SYNSETS = 'synsets/';
var SYNSET = '/synset';
var SEARCH = '/search';
var QUERY_PARAM = 'lemma';
var ATTRIBUTES = '/attributes';
var RELATIONS = '/relations';

var requestCount = 0;

exports.xmlToWordnet = handleSynsetId;//handleWord;

// function handleWord(word, callback) {
// 	getWordSense(word).then(function(result) {
// 		console.log(result);
// 		callback(result);
// 	}, function(error) {
// 		console.log("got some error :(" + error);
// 	});
// };

function handleSynsetId(senseId, callback) {
	getSense(senseId).then(function(senses) {
		return Promise.resolve(getAttributes(senses));
	}).then(function(result) {
			return Promise.resolve(parseSynsets(result));
		}).then(function(result) {
			callback(result);
		}, function(error) {
			console.log("error: " + JSON.stringify(error));
			callback(null);
		});
}
function getSense(synsetId) {
	return reqP({
		  url: BASE_URL + SYNSETS + synsetId + SENSE,
		  method: "get",
		  json : true,
		  timeout: 50000
	}).then(function(result) {
		return Promise.resolve({
			"id":result[0].id,
			"word": result[0].lemma.word
		});
	})
};

function getSynset(senseId) {
	return reqP({
		  url: BASE_URL + SENSES + senseId + SYNSET,
		  method: "get",
		  json : true,
		  timeout: 50000
	}).then(function(result) {
		return Promise.resolve(result.id);
	}, function(error) {
		console.log("error in synset:" + senseId);
	})
};



function checkProperObject(argument) {
	if(argument) {
		return (argument.link != '' || argument.definition != '' || argument.sample != '');
	}
	return false;
}

function getWordSense(argument) {
	requestCount = requestCount + 1;
	var word = argument;
	return reqP({
		  url: BASE_URL + SENSES + SEARCH,
		  method: "get",
		  qs : {
		  	lemma : word
		  },
		  json : true,
		  timeout: 50000
	}).then(function(result) {
		return Promise.all(result.content.map(getAttributes)).then(function(parsed) {
			return Promise.all(parsed.map(function(res) {
				res.word=word;
				return res;
			})).then(function(result) {
				return Promise.all(result.map(parseSynsets));
			});
		});
	}, function(err) {
		console.log("word sense error: " + err);
	});
};

function parseRelations(argument) {
	return reqP({
		url: BASE_URL + SENSES + argument.id + RELATIONS,
		method: "get",
		json: true,
		timeout: 50000
	}).then(function(result) {
		return argument;
	})
}

function parseSynsets(argument) {
	requestCount++;
	return reqP({
		url: BASE_URL + SENSES + argument.id + SYNSET,
		method: "get",
		json: true,
		timeout: 50000
	}).then(function(result) {
		return parseSynsetRelations(argument, result.id).then(function(res) {
			return res;
		});
	});
};

function parseSynsetRelations(argument, id) {
	requestCount++;
	return reqP({
		url: BASE_URL + SYNSETS + id + ATTRIBUTES,
		method: "get",
		json: true,
		timeout: 50000
	}).then(function(result) {
		requestCount++;
		// console.log(result);
		argument = parseSynsetResponseIfExist(argument, result);
		return reqP({
		url: BASE_URL + SYNSETS + id + RELATIONS,
		method: "get",
		json: true,
		timeout: 50000
	}).then(function(result) {
		var relations = [];
		var groupedRelations = groupRelationsById(result);
		for (var i = 0; i < result.length; i++) {
			relations.push(getSynsetRelationsParsed(result[i], argument, id));
		};
		return Promise.all(relations).then(function(relationsParsed) {
			for (var i = 0; i < relationsParsed.length; i++) {
				putRelationInProperGroup(groupedRelations, relationsParsed[i]);
			}
			argument.relations = groupedRelations;
			return Promise.resolve(argument);
		});
	});
});
};

function putRelationInProperGroup(groupedRelations, relationToAdd) {
	var relation = findInArray(groupedRelations, relationToAdd.id);
	relation.items.push(relationToAdd.relation);
}

function groupRelationsById(relations) {
	var grouped = [];

	for (var i = 0; i < relations.length; i++) {
		var relation = relations[i];
		var relationNew = {
			"name":relation.relation.name,
			"id" : relation.relation.id,
			"items" : []
		};
		if(!findInArray(grouped, relationNew.id)){
			grouped.push(relationNew);
		}
	}
	return grouped;
}

function findInArray(array, id) {
	for (var i = 0; i < array.length; i++) {
		if(array[i].id == id){
			return array[i];
		}
	}
	return null;
}

function getSynsetRelationsParsed(relation, argument, id) {
	var synId = relation.synsetTo.id;
	if(synId == id){
		synId = relation.synsetFrom.id;
	}
	return getWordForRelation(synId, relation.relation.id);
}

function getWordForRelation(id, relationId) {
	requestCount++;
	return reqP({
		url: BASE_URL + SYNSETS + id + SENSE,
		method: "get",
		json: true,
		timeout: 50000
	}).then(function(result) {
		return getSynset(result[0].id).then(function(id) {
			requestCount++;
			var rel = {
				"id" : relationId,
				"relation":{
					"word" : result[0].lemma.word,
					"wordId": id
				}
			}
			return rel;
		},function(error) {
			var rel = {
				"id" : relationId,
				"relation":{
					"word" : result[0].lemma.word,
					"wordId": -1
				}
			}
			return rel;
		})
	});
}

function parseSynsetResponseIfExist(argument, result) {
	for(i=0; i<result.length;i++){
		var typeName = result[i].type.typeName.toString();
		if(typeName == "link"){
			argument.link = result[i].value;
		}else if(typeName == "definition"){
			argument.definition = result[i].value;
		}else if(typeName == "use_cases"){
			argument.sample = result[i].value;
		}
	}

	return argument;
	
}

function getAttributes(sense, callback) {
	requestCount = requestCount + 1;
	return reqP({
		url: BASE_URL + SENSES + sense.id + ATTRIBUTES,
		method: "get",
		json: true,
		timeout: 50000
	}).then(function(result) {
		return Promise.resolve(parseResponse(result, sense));
	},
	function(err) {
		console.log("attributes error: " + err);

	});
};

function parseResponse(response, sense) {
	var obj = {
		"id":sense.id,
		"word":sense.word,
		link:"",
		definition:"",
		sample:""
	};
	for(i=0; i<response.length;i++){
		var typeName = response[i].type.typeName.toString();
		if(typeName == "link"){
			obj.link = response[i].value;
		}else if(typeName == "definition"){
			obj.definition = response[i].value;
		}else if(typeName == "use_cases"){
			obj.sample = response[i].value;
		}
	}
	return Promise.resolve(obj);
}