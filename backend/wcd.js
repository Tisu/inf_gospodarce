var request = require('request')
var wsdBaseUrl = "http://ws.clarin-pl.eu/nlprest/base";
var uploadUrl = "/upload/";
var startTaskWsd2 = "/startTask/wsd2/";
var startTaskUrl = "/startTask/wcrft2/";
var getStatus = "/getStatus/";
var downloadResult = "/download/";
var Promise = require('promise');
var parseString = require('xml2js').parseString;
var parseXml = Promise.denodeify(parseString);


function loadText(text, callback) {
	console.log(text);
	request({
  url: wsdBaseUrl + uploadUrl,
  body:text,
  method: "POST",
  headers:{
  	'Content-Type':'text/plain'
  },
  timeout: 10000
	}, function(error, response, body) {
	if(error){
		console.log("error");
		callback("error:" +  error);
	}else{
		startWCRFT2(body, callback);
		console.log("file id: " + body);
		// callback(body);
	}
})
};
exports.loadText=loadText;

function startWCRFT2(fileId, callback) {
	request(wsdBaseUrl + startTaskUrl + fileId, function(error, response, body) {
		if (error) {
			console.log("error starting wcrft2: " + error);
   			callback(error);
		}else{
			console.log("started task wcrft2. Waiting 1s to check status");
			setTimeout(function() {
				checkStatus(body, callback);
			}, 1000);
		}
	});
};

function checkStatus(taskId, callback) {
	console.log("checking status...");
	request(wsdBaseUrl + getStatus + taskId, function(error, response, body) {
		if(error){
			console.log("status error: " + error);
			callback(error);
		}else{
			jsonresp = JSON.parse(body);
			if(jsonresp.status == "DONE"){
				console.log("status done. Starting wsd2 task:" + jsonresp.value);
				//downloadOutputFile(jsonresp.value, callback)
				startWsd2(jsonresp.value, callback)
			}else{
				console.log("file not ready. Waiting to check again... status: " + jsonresp.status);
				setTimeout(function() {
					checkStatus(taskId, callback);
				}, 2000);
			}
		}
	});
};

function startWsd2(outputWcrft2Id, callback)
 {
 	request(wsdBaseUrl + startTaskWsd2 + outputWcrft2Id, function(error, response, body) {
		if(error){
			console.log("status error: " + error);
			callback(error);
		}else{
			console.log("started task wsd2. Waiting 1s to check status");
			setTimeout(function() {
				checkWsdStatus(body, callback);
			}, 1000);
		}
	});
 }

 function checkWsdStatus(taskId, callback) {
	console.log("checking status...taskId: " + taskId);
	request(wsdBaseUrl + getStatus + taskId, function(error, response, body) {
		if(error){
			console.log("status error: " + error);
			callback(error);
		}else{
			jsonresp = JSON.parse(body);
			if(jsonresp.status == "DONE"){
				console.log("status done. Starting loading file:" + jsonresp.value);
				downloadOutputFile(jsonresp.value, callback)
			}else{
				console.log("file not ready. Waiting to check again... status: " + jsonresp.status);
				setTimeout(function() {
					checkWsdStatus(taskId, callback);
				}, 2000);
			}
		}
	});
};

function downloadOutputFile(outputId, callback) {
		request(wsdBaseUrl + downloadResult + outputId, function(error, response, body) {
			if (error) {
				console.log("error loading result: " + error);
    			callback(error);
  			}else{
  				parseXml(body).then(function(result) {
  					return Promise.all(result.chunkList.chunk.map(parseSentences)).then(function(result) {
						return Promise.resolve(flattenArray(result));
  					}).then(function(wordDef) {	
  						console.log("success: " +  JSON.stringify(wordDef));
						callback({"definitions" : wordDef})
									// "word" : });
					}, function(err) {
						callback(err);
						console.log("error: " + err);
					});
  				});
  			}
  		});
};

function parseSentences(sentences) {
	return Promise.all(sentences.sentence.map(parseWords)).then(function(result) {
		return Promise.resolve(flattenArray(result));
	});
};

function flattenArray(array) {
	console.log("flatten array");
	var merged = [];
	for (var i = 0; i < array.length; i++) {
		merged = merged.concat(array[i]);
	}
	return merged;
}

function parseWords(words) {
	var objects = [];
	console.log(JSON.stringify(words));
	for (var i = 0; i < words.tok.length; i++) {
		var word = words.tok[i];
		var ids = -1;
		console.log(JSON.stringify(word));
		if(word.prop && word.prop.length > 0) {
			console.log("id: " + word.prop[0]._)
			ids = word.prop[0]._;
		}else{
			continue;
		}
		objects.push({"word": word.lex[0].base[0],
								"id": ids});
	}
	
	return Promise.resolve(objects);
};
