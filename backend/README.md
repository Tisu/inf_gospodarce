INFORMATYKA W GOSPODARCE

Słowosieć

Aplikacja ta ma na celu umożliwienie wyświetlenie zasobów zgromadzonych w słowosieci
w formie interaktywnego grafu wyświetlonego w przeglądarce internetowej. 


Struktura plików:
./backend - główny katalog zawierający cały kod aplikacji,<br>
./backend/server.js - kod źródłowy odpowiedzialny za konfigurację, oraz uruchomienie serwera aplikacji,
./backend/routes.js - plik zawierający definicje obsługiwanych ścieżek,
./backend/package.json - plik zawierający wszystkie zależności aplikacji,
./public/ - folder zawierający wszystkie udostępnione pliki. Każdy z plików można pobrać statycznie poprzez: server/plik. Dodatkowo plik index.htm jest domyślnym plikiem, który zostanie otwarty w przypadku dodania dowolnej ścieżki.

Jak uruchomić:
1. Sciągnij i zainstaluj NodeJS na swoją platformę(https://nodejs.org/en/#download),
2. Upewnij się, że nodeJS działa(node -v),
3. Wraz z instalacją NodeJS powinien być zainstalowany menadżer pakietów npm(npm -v)
4. Jeżeli oba programy zostają znalezione ściągamy zależności wymagane w projekcie:
	I. cd backend
	II. npm install
	III. ...
	IV. profit
5. Uruchamiamy serwer:
	I. node server.js
	II. W przeglądarce powinna być dostępna strona: localhost:8080
6. W przypadku jakiś problemów -> piszcie do mnie
