var request = require('request')
var reqP = require('request-promise');

var parseString = require('xml2js').parseString;
var Promise = require('promise');
var BASE_URL = 'http://api.slowosiec.clarin-pl.eu:8080/plwordnet-api/'
var SENSES = 'senses/';
var SENSE = '/senses';
var SYNSETS = 'synsets/';
var SYNSET = '/synset';
var SEARCH = '/search';
var QUERY_PARAM = '?lemma=';
var ATTRIBUTES = '/attributes';
var RELATIONS = '/relations';

var requestCount = 0;

var parseXml = Promise.denodeify(parseString);
exports.xmlToWordnet = handleXmlResponse;

function handleXmlResponse(xml, callback) {
	parseXml(xml).then(function(result) {
		return Promise.all(result.chunkList.chunk.map(parseSentences)).then(function(wordDef) {
			console.log("requests sent: " + requestCount);
			definitions = wordDef[0].filter(function(value) {
				// console.log(value);
				var val = checkProperObject(value);
				// console.log("val: " + val);
				return val;
			});
			callback(null, definitions);
		}, function(err) {
			callback(err, null);
			console.log("error: " + err);
		});
	});
};

function checkProperObject(argument) {
	if(argument) {
		return (argument.link != '' || argument.definition != '' || argument.sample != '');
	}
	return false;
}
function parseSentences(sentences) {
	return Promise.all(sentences.sentence.map(parseWords)).then(function(result) {
		return Promise.resolve(result[0]);
	});
};

function parseWords(words) {
	return Promise.all(words.tok.map(getWordSense)).then(function(result) {
		return Promise.resolve(result);
	});
};

function getWordSense(argument) {
	requestCount = requestCount + 1;
	var word = argument.lex[0].base[0];
	return reqP({
		  url: BASE_URL + SENSES + SEARCH + QUERY_PARAM + word,
		  method: "get",
		  json : true,
		  timeout: 50000
	}).then(function(result) {
		return Promise.all(result.content.map(getAttributes)).then(function(parsed) {
			return Promise.all(parsed.map(function(res) {
				res.word=word;
				return res;
			})).then(function(result) {
				return Promise.all(result.map(parseSynsets));
			});
		});
	}, function(err) {
		console.log("word sense error: " + err);
	});
};

function parseRelations(argument) {
	return reqP({
		url: BASE_URL + SENSES + argument.id + RELATIONS,
		method: "get",
		json: true,
		timeout: 50000
	}).then(function(result) {
		console.log("relation: " + JSON.stringify(result));
		return argument;
	})
}

function parseSynsets(argument) {
	requestCount++;
	return reqP({
		url: BASE_URL + SENSES + argument.id + SYNSET,
		method: "get",
		json: true,
		timeout: 50000
	}).then(function(result) {
		return parseSynsetRelations(argument, result.id).then(function(res) {
			console.log(JSON.stringify(res));
			return res;
		});
	});
};

function parseSynsetRelations(argument, id) {
	requestCount++;
	return reqP({
		url: BASE_URL + SYNSETS + id + ATTRIBUTES,
		method: "get",
		json: true,
		timeout: 50000
	}).then(function(result) {
		requestCount++;
		argument = parseSynsetResponseIfExist(argument, result);
		return reqP({
		url: BASE_URL + SYNSETS + id + RELATIONS,
		method: "get",
		json: true,
		timeout: 50000
	}).then(function(result) {
		// console.log("synset relation:\n " + JSON.stringify(result));
		// console.log("\n\n\n");
		var relations = [];
		for (var i = 0; i < result.length; i++) {
			relations.push(getSynsetRelationsParsed(result[i], argument, id));
		};
		return Promise.all(relations).then(function(relationsParsed) {
			argument.relations = relationsParsed;
			return Promise.resolve(argument);
		});
	});
});
};

function getSynsetRelationsParsed(relation, argument, id) {
	var rel = {
		"name" : relation.relation.name,
		"id" : relation.relation.id,
		"word" : "",
		"wordId":-1
	}
	var synId = relation.synsetTo.id;
	if(synId == id){
		synId = relation.synsetFrom.id;
	}
	return getWordForRelation(synId, rel);
}

function getWordForRelation(id, rel) {
	requestCount++;
	return reqP({
		url: BASE_URL + SYNSETS + id + SENSE,
		method: "get",
		json: true,
		timeout: 50000
	}).then(function(result) {
		// console.log(result);
		requestCount++;
		rel.word = result[0].lemma.word;
		rel.wordId = result[0].lemma.id;
		return rel;
	});
}

function parseSynsetResponseIfExist(argument, result) {
	for(i=0; i<result.length;i++){
		var typeName = result[i].type.typeName.toString();
		if(typeName == "link"){
			argument.link = result[i].value;
		}else if(typeName == "definition"){
			argument.definition = result[i].value;
		}else if(typeName == "use_cases"){
			argument.sample = result[i].value;
		}
	}

	return argument;
	
}

function getAttributes(sense, callback) {
	requestCount = requestCount + 1;
	return reqP({
		url: BASE_URL + SENSES + sense.id + ATTRIBUTES,
		method: "get",
		json: true,
		timeout: 50000
	}).then(function(result) {
		return Promise.resolve(parseResponse(result, sense.id));
	},
	function(err) {
		console.log("attributes error: " + err);

	});
};

function parseResponse(response, id) {
	var obj = {
		"id":id,
		"word":"",
		link:"",
		definition:"",
		sample:""
	};
	for(i=0; i<response.length;i++){
		var typeName = response[i].type.typeName.toString();
		if(typeName == "link"){
			obj.link = response[i].value;
		}else if(typeName == "definition"){
			obj.definition = response[i].value;
		}else if(typeName == "use_cases"){
			obj.sample = response[i].value;
		}
	}
	return Promise.resolve(obj);
}