var request = require('request')
var parseString = require('xml2js').parseString;
var BASE_URL = 'http://api.slowosiec.clarin-pl.eu:8080/plwordnet-api/'
var SENSES = 'senses/';
var SEARCH = '/search';
var QUERY_PARAM = '?lemma=';
var ATTRIBUTES = '/attributes';
function handleXmlResponse(xml, callback) {
	parseString(xml, function(err, result) {
		if(err){
			console.log(err);
		}else{
			result.chunkList.chunk.forEach(function(value) {
				parseSentences(value.sentence, callback);
				// value.sentence.forEach(function(value) {
				// 	value.tok.forEach(function(value) {
				// 		console.log(JSON.stringify(value.lex[0].base) + '\n\n');
				// 		getWordSense(value.lex[0].base, callback);
				// 	});
				// });
			});
			// console.log(JSON.stringify(result));
		}
	});
};
exports.xmlToWordnet = handleXmlResponse;

function parseSentences(sentences, callback) {
	var objects = [];
	sentences.forEach(function(sentence) {
		console.log(sentence);
		parseWords(sentence, function(res) {
			console.log("res: " + res);
			objects.push(res);
			if(objects.length == sentences.length){
				callback(objects);
			}
		});
	});
	// for (var i = 0; i < sentences.length; i++) {
	// 	objects.push(parseWords[sentences[i]]);
	// 	console.log("sentence: " + i);
	// }
	// console.log(objects);
	// callback(objects);
};

function parseWords(words, callback) {
	var objects = [];
	console.log("parsing words: " + JSON.stringify(words.tok));
	var wordSenseCallback = function(err, wordSense) {
		if(err){
			callback(err);
		}else{
			objects.push(wordSense);
			console.log("Word Sense: " + wordSense);
			if(objects.length == words.length){
				callback(objects);
			}else{
				getWordSense(words.tok[objects.length].lex[0].base, wordSenseCallback);
			}
		}

	}
	getWordSense(words.tok[0].lex[0].base, wordSenseCallback);
};
function getWordSense(argument, callback) {
	console.log(argument);
	request({
  url: BASE_URL + SENSES + SEARCH + QUERY_PARAM + argument,
  method: "get",
  timeout: 10000
	}, function(error, response, body) {
		if(error){
			console.log(error);
		}else{
			// console.log(body);
			var senses = JSON.parse(body);
			console.log("senses: " + JSON.stringify(senses.content));
			var objects = [];
			senses.content.forEach(function(sense) {
				getAttributes(sense.id, function(attr) {
					attr.word = argument[0];
					objects.push(attr);
					console.log("attribute: " + JSON.stringify(attr));
					if(objects.length == senses.content.length){
						console.log(objects);
						callback(objects);
					}
				});
			});
			// for (var i = 0; i < senses.length; i++) {
			// 	objects.push(getAttributes(senses[i].id));
			// }
			// console.log("senses" + objects);
			// return objects;
			// JSON.parse(body).content.forEach(function(value) {
			// 	console.log(value.id);
			// 	getAttributes(value.id, callback);
			// });
			// console.log( '\n\n' + body + '\n\n' );
		}
	}
)};

function getAttributes(id, callback) {
request({
  url: BASE_URL + SENSES + id + ATTRIBUTES,
  method: "get",
  timeout: 10000
	}, function(error, response, body) {
		if(error){
			console.log(error);
		}else{
			console.log("attrs:"+body);
			callback(parseResponse(body));
			// JSON.parse(body).content.forEach(function(value) {
			// 	console.log(value.id);
			// });
			// console.log( '\n\n' + body + '\n\n' );
		}
	}
)};


function parseResponse(response) {
	resp = JSON.parse(response);
	var obj = {
		"word":"",
		link:"",
		definition:"",
		sample:""
	};
	for(i=0; i<resp.length;i++){
		var typeName = resp[i].type.typeName.toString();
		if(typeName == "link"){
			obj.link = resp[i].value;
		}else if(typeName == "definition"){
			obj.definition = resp[i].value;
		}else if(typeName == "use_cases"){
			obj.sample = resp[i].value;
		}
	}
	// console.log(obj);
	console.log(JSON.stringify(obj));
	return obj;
}