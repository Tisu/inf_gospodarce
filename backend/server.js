var morgan = require('morgan')
var bodyParser = require('body-parser')
var express = require('express');

var app = express();

//all the files from /public dir are available to load as /filename
app.use(express.static(__dirname + '/public'));
//enable request logging to console
app.use(morgan('dev'));
app.use(bodyParser.json());

//register all the required routes
require('./routes')(app);

app.listen(8080);
console.log("Started server on port 8080");
